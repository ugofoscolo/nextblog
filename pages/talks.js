import React from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
// react components for routing our app without refresh
import Link from "next/link";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
// @mui/icons-material
// core components
import Header from "~/components/Header/Header.js";
import Badge from "~/components/Badge/Badge.js";
import HeaderLinks from "~/components/Header/HeaderLinks.js";
import Footer from "~/components/Footer/Footer.js";
import GridContainer from "~/components/Grid/GridContainer.js";
import GridItem from "~/components/Grid/GridItem.js";
import Parallax from "~/components/Parallax/Parallax.js";
import Date from '~/components/date'
// sections for this page
import styles from "~/styles/jss/nextjs-material-kit/pages/components.js";
import { getSortedTalksData } from '~/lib/talks';
import utilStyles from '~/styles/utils.module.css';
const useStyles = makeStyles(styles);

export default function Talks({ allTalksData }) {
  const classes = useStyles();
  return (
    (<div>
      <Header
        brand="Cristiano's Blog"
        rightLinks={<HeaderLinks />}
        fixed
        color="white"
        changeColorOnScroll={{
          height: 400,
          color: "white",
        }}
      />
      <Parallax image="/img/talks.jpg">
        <div className={classes.container}>
          <GridContainer>
            <GridItem>
              <div className={classes.brandbg}>
              <div className={classes.brand}>
                <h1 className={classes.title}>Talks</h1>
                <h3 className={classes.subtitle}>
                <i>Il mondo è pieno di cose ovvie che nessuno si prende mai la cura di osservare.</i><br />
                </h3>
                <h4>Arthur Conan Doyle.</h4>
              </div>
              </div>
            </GridItem>
          </GridContainer>
        </div>
      </Parallax>
      <div className={classNames(classes.main, classes.mainRaised)}>
        <div className={classes.sections}>
          <div className={classes.container}>
            <ul className={utilStyles.list}>
            {allTalksData.map(({ id, date, title, tags }) => (
              <li className={utilStyles.listItem} key={id}>
                <Link href={`/talks/${id}`} legacyBehavior>
                  {title}
                </Link>
                <small className={utilStyles.lightText}>
                  &nbsp;- <Date dateString={date} />
                </small>
                <br />
                {tags.map((tag) => (
                  <Badge color="primary">{tag.toString()}</Badge>
                ))}
              </li>
            ))}
            </ul>
          </div>
        </div>
      </div>
      <Footer />
    </div>)
  );
}

export async function getStaticProps() {
  const allTalksData = getSortedTalksData()
  return {
    props: {
      allTalksData
    }
  }
}
