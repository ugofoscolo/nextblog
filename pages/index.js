import React from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
// @mui/icons-material
import Camera from "@mui/icons-material/Camera";
import Palette from "@mui/icons-material/Palette";
import Favorite from "@mui/icons-material/Favorite";
// core components
import Header from "~/components/Header/Header.js";
import Footer from "~/components/Footer/Footer.js";
import Button from "~/components/CustomButtons/Button.js";
import GridContainer from "~/components/Grid/GridContainer.js";
import GridItem from "~/components/Grid/GridItem.js";
import HeaderLinks from "~/components/Header/HeaderLinks.js";
import NavPills from "~/components/NavPills/NavPills.js";
import Parallax from "~/components/Parallax/Parallax.js";

import styles from "~/styles/jss/nextjs-material-kit/pages/indexPage.js";

const useStyles = makeStyles(styles);

export default function ProfilePage(props) {
  const classes = useStyles();
  const { ...rest } = props;
  const imageClasses = classNames(
    classes.imgRaised,
    classes.imgRoundedCircle,
    classes.imgFluid
  );
  const navImageClasses = classNames(classes.imgRounded, classes.imgGallery);
  return (
    <div>
      <Header
        color="white"
        brand="Cristiano's Blog"
        rightLinks={<HeaderLinks />}
        fixed
        changeColorOnScroll={{
          height: 200,
          color: "white",
        }}
        {...rest}
      />
      <Parallax small filter className={classes.topimage} image="/img/homepage.jpg" />
      <div className={classNames(classes.main, classes.mainRaised)}>
        <div className={classes.sections}>
          <div className={classes.container}>
            <GridContainer justifyContent="center">
              <GridItem xs={12} sm={12} md={6}>
                <div className={classes.profile}>
                  <div>
                    <img
                      src="/img/caricature_sqr.jpg"
                      alt="..."
                      className={imageClasses}
                    />
                  </div>
                  <div className={classes.name}>
                    <h3 className={classes.title}>Cristiano Casella</h3>
                    <h6></h6>
                    <a href="https://gitlab.com/ccasella" target="_blank" rel='noreferrer noopener'><Button justIcon link className={classes.margin5}>
                      <i className={"fab fa-gitlab"} />
                    </Button></a>
                    <a href="https://gitlab.com/ugofoscolo" target="_blank" rel='noreferrer noopener'><Button justIcon link className={classes.margin5}>
                      <i className={"fab fa-gitlab"} />
                    </Button></a>
                    <a href="https://github.com/cristianocasella" target="_blank" rel='noreferrer noopener'><Button justIcon link className={classes.margin5}>
                      <i className={"fab fa-github"} />
                    </Button></a>
                    <a href="https://www.linkedin.com/in/cristianocasella/" target="_blank" rel='noreferrer noopener'><Button justIcon link className={classes.margin5}>
                      <i className={"fab fa-linkedin"} />
                    </Button></a>
                  </div>
                </div>
              </GridItem>
            </GridContainer>
            <div className={classes.description}>
              <p>
                Prima o poi ci scrivo qualcosa in questo spazio, promesso.{" "}
              </p>
              <br />
              <br />
              <br />
              <br />
              <br />
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
}
