import React from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
// react components for routing our app without refresh
import Link from "next/link";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
// @mui/icons-material
// core components
import Header from "~/components/Header/Header.js";
import HeaderLinks from "~/components/Header/HeaderLinks.js";
import Footer from "~/components/Footer/Footer.js";
import GridContainer from "~/components/Grid/GridContainer.js";
import GridItem from "~/components/Grid/GridItem.js";
import Parallax from "~/components/Parallax/Parallax.js";
// sections for this page
import styles from "~/styles/jss/nextjs-material-kit/pages/components.js";
import { GraphLog,  GraphNcasi , GraphTest } from "~/pages-sections/Components-Sections/SectionCovidGraph.js";
import { GuaritiDeceduti, NuoviDati , PercentualiGenerali, SituazioneRicoveri, TestCovid , UltimiDati } from "~/pages-sections/Components-Sections/SectionCovid.js";

const useStyles = makeStyles(styles);

export default function Components(props) {
  const classes = useStyles();
  const { ...rest } = props;
  return (
    <div>
      <Header
        brand="Cristiano's Blog"
        rightLinks={<HeaderLinks />}
        fixed
        color="white"
        changeColorOnScroll={{
          height: 400,
          color: "white",
        }}
        {...rest}
      />
      <Parallax image="/img/covid.jpg">
        <div className={classes.container}>
          <GridContainer>
            <GridItem>
              <div className={classes.brandbg}>
              <div className={classes.brand}>
                <h1 className={classes.title}>Covid</h1>
                <h3 className={classes.subtitle}>
                <i>&Egrave; nella crisi che sorge l&apos;inventiva, le scoperte e le grandi strategie. Chi supera la crisi  supera s&eacute; stesso senza essere superato.</i><br />
                </h3>
                <h4>Albert Einstein</h4>
              </div>
              </div>
            </GridItem>
          </GridContainer>
        </div>
      </Parallax>

      <div className={classNames(classes.main, classes.mainRaised)}>
        <div className={classes.sections}>
          <div className={classes.container}>
            <p>Questa pagina si aggiorna automaticamente con i dati pubblicati dal ministero su <Link href="https://github.com/pcm-dpc/COVID-19">GitHub</Link> e crea dei grafici per una più facile consultazione di essi.</p>
            <h2>Ultimi Dati</h2>
            <UltimiDati />
            <h2>Percentuali Generali</h2>
            <PercentualiGenerali />
            <h2>Nuovi Dati</h2>
            <GraphNcasi />
            <GraphLog />
            <NuoviDati />
            <h2>Risultati Test Covid</h2>
            <GraphTest />
            <TestCovid />
            <h2>Guarigioni e Decessi</h2>
            <GuaritiDeceduti />
            <h2>Situazione Ricoveri</h2>
            <SituazioneRicoveri />
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
}
