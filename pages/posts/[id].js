import React from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
// react components for routing our app without refresh
import Link from "next/link";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
// @mui/icons-material
// core components
import Header from "~/components/Header/Header.js";
import Badge from "~/components/Badge/Badge.js";
import HeaderLinks from "~/components/Header/HeaderLinks.js";
import Footer from "~/components/Footer/Footer.js";
import GridContainer from "~/components/Grid/GridContainer.js";
import GridItem from "~/components/Grid/GridItem.js";
import Parallax from "~/components/Parallax/Parallax.js";
// sections for this page
import { getAllPostIds, getPostData } from '~/lib/posts';
import styles from "~/styles/jss/nextjs-material-kit/pages/components.js";

const useStyles = makeStyles(styles);

import Date from '~/components/date'

export default function Post({props, postData}) {
  const classes = useStyles();
  const { ...rest } = props || {};
  const listone = listTags(postData);
  return (
    <div>
      <Header
        brand="Cristiano's Blog"
        rightLinks={<HeaderLinks />}
        fixed
        color="white"
        changeColorOnScroll={{
          height: 400,
          color: "white",
        }}
        {...rest}
      />
      <Parallax image="/img/posts.jpg">
        <div className={classes.container}>
          <GridContainer>
            <GridItem>
              <div className={classes.brandbg}>
              <div className={classes.brand}>
                <h1 className={classes.title}>{postData.title}</h1>
                <h3 className={classes.subtitle}>
                <i><Date dateString={postData.date} /></i>
                </h3>
                <h4>
                    {listone}
                </h4>
              </div>
              </div>
            </GridItem>
          </GridContainer>
        </div>
      </Parallax>

      <div className={classNames(classes.main, classes.mainRaised)}>
	  <div className={classes.sections}>
      <div className={classes.container} dangerouslySetInnerHTML={{ __html: postData.contentHtml }} />
    </div>
      </div>
      <Footer />
    </div>
  );
}

export async function getStaticPaths() {
  const paths = getAllPostIds()
  return {
    paths,
    fallback: false
  }
}

export async function getStaticProps({ params }) {
  const postData = await getPostData(params.id)
  return {
    props: {
      postData
    }
  }
}

export function listTags(postData) {
  const listTags = postData.tags.map((tag) =>
<Badge color="primary">{tag.toString()}</Badge>
  )
  return (
    listTags
  )
}
