/*!

=========================================================
* NextJS Material Kit v1.2.0 based on Material Kit Free - v2.0.2 (Bootstrap 4.0.0 Final Edition) and Material Kit React v1.8.0
=========================================================

* Product Page: https://www.creative-tim.com/product/nextjs-material-kit
* Copyright 2021 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/nextjs-material-kit/blob/main/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from "react";
import ReactDOM from "react-dom";
import App from "next/app";
import Head from "next/head";
import Router from "next/router";
import Script from "next/script";
import * as gtag from "~/lib/ga/gtag.js";

import PageChange from "~/components/PageChange/PageChange.js";

import "~/styles/scss/nextjs-material-kit.scss?v=1.2.0";

Router.events.on("routeChangeStart", (url) => {
  console.log(`Loading: ${url}`);
  document.body.classList.add("body-page-transition");
  ReactDOM.render(
    <PageChange path={url} />,
    document.getElementById("page-transition")
  );
});
Router.events.on("routeChangeComplete", () => {
  ReactDOM.unmountComponentAtNode(document.getElementById("page-transition"));
  document.body.classList.remove("body-page-transition");
});
Router.events.on("routeChangeError", () => {
  ReactDOM.unmountComponentAtNode(document.getElementById("page-transition"));
  document.body.classList.remove("body-page-transition");
});

export default class MyApp extends App {
  componentDidMount() {
    let comment = document.createComment(`

=========================================================
* NextJS Material Kit v1.2.0 based on Material Kit Free - v2.0.2 (Bootstrap 4.0.0 Final Edition) and Material Kit React v1.8.0
=========================================================

* Product Page: https://www.creative-tim.com/product/nextjs-material-kit
* Copyright 2021 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/nextjs-material-kit/blob/main/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

`);
    document.insertBefore(comment, document.documentElement);
  }
  render() {
    const { Component, pageProps } = this.props;

    return (
      <React.Fragment>
        <Head>
          <meta
            name="viewport"
            content="width=device-width, initial-scale=1, shrink-to-fit=no"
          />
          <title>Cristiano Casella</title>
        </Head>
        {/* Global Site Tag (gtag.js) - Google Analytics */}
        <Script
          strategy="afterInteractive"
          src={`https://www.googletagmanager.com/gtag/js?id=${gtag.GA_TRACKING_ID}`}
        />
        <Script
          id="secondScript"
          strategy="afterInteractive"
          dangerouslySetInnerHTML={{
            __html: `
              window.dataLayer = window.dataLayer || [];
              function gtag(){dataLayer.push(arguments);}
              gtag('js', new Date());
              gtag('config', '${gtag.GA_TRACKING_ID}', {});
            `,
          }}
        /> 
        <Script
            id="iubendaScript"
            strategy="afterInteractive"
            dangerouslySetInnerHTML={{
              __html: `
                var _iub = _iub || []
                _iub.csConfiguration = {
                  "askConsentAtCookiePolicyUpdate":true,
                  "floatingPreferencesButtonDisplay":"bottom-right",
                  "invalidateConsentWithoutLog":true,
                  "perPurposeConsent":true,
                  "siteId":2955538,
                  "whitelabel":false,
                  "cookiePolicyId":75297402,
                  "lang":"it",
                  "banner":{
                    "acceptButtonDisplay":true,
                    "closeButtonDisplay":false,
                    "customizeButtonDisplay":true,
                    "explicitWithdrawal":true,
                    "listPurposes":true,
                    "position":"float-top-center",
                    "rejectButtonDisplay":true
                  }
                };
              `,
            }}
          />
          <Script
            id="secondIubendaScript"
            strategy="afterInteractive"
            src="//cdn.iubenda.com/cs/beta/iubenda_cs.js"
          />
        <Component {...pageProps} />
      </React.Fragment>
    );
  }
}
