import React from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
// react components for routing our app without refresh
import Link from "next/link";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
// @mui/icons-material
// core components
import Header from "~/components/Header/Header.js";
import Badge from "~/components/Badge/Badge.js";
import HeaderLinks from "~/components/Header/HeaderLinks.js";
import Footer from "~/components/Footer/Footer.js";
import GridContainer from "~/components/Grid/GridContainer.js";
import GridItem from "~/components/Grid/GridItem.js";
import Parallax from "~/components/Parallax/Parallax.js";
import Date from '~/components/date'
// sections for this page
import styles from "~/styles/jss/nextjs-material-kit/pages/components.js";
import { getSortedPostsData } from '~/lib/posts';
import utilStyles from '~/styles/utils.module.css';
const useStyles = makeStyles(styles);

export default function Posts({ allPostsData }) {
  const classes = useStyles();
  return (
    (<div>
      <Header
        brand="Cristiano's Blog"
        rightLinks={<HeaderLinks />}
        fixed
        color="white"
        changeColorOnScroll={{
          height: 400,
          color: "white",
        }}
      />
      <Parallax image="/img/posts.jpg">
        <div className={classes.container}>
          <GridContainer>
            <GridItem>
              <div className={classes.brandbg}>
              <div className={classes.brand}>
                <h1 className={classes.title}>Posts</h1>
                <h3 className={classes.subtitle}>
                <i>La teoria è quando si sa tutto ma non funziona niente. La pratica è quando tutto funziona ma non si sa il perché.<br />In ogni caso si finisce sempre con il coniugare la teoria con la pratica: non funziona niente e non si sa il perché.</i><br />
                </h3>
                <h4>Albert Einstein</h4>
              </div>
              </div>
            </GridItem>
          </GridContainer>
        </div>
      </Parallax>
      <div className={classNames(classes.main, classes.mainRaised)}>
        <div className={classes.sections}>
          <div className={classes.container}>
          <ul className={utilStyles.list}>
          {allPostsData.map(({ id, date, title, tags}) => (
            <li className={utilStyles.listItem} key={id}>
              <Link href={`/posts/${id}`} legacyBehavior>
                {title}
              </Link>
              <small className={utilStyles.lightText}>
                &nbsp;- <Date dateString={date} />
              </small>
              <br />
              {tags.map((tag) => (
              <Badge key={id} color="primary">{tag.toString()}</Badge>
              ))}
            </li>
          ))}
        </ul>
          </div>
        </div>
      </div>
      <Footer />
    </div>)
  );
}

export async function getStaticProps() {
  const allPostsData = getSortedPostsData()
  return {
    props: {
      allPostsData
    }
  }
}
