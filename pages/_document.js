import React from "react";
import Document, { Html, Head, Main, NextScript } from "next/document";
import { ServerStyleSheets } from "@material-ui/core/styles";

class MyDocument extends Document {
  render() {
    return (
      <Html lang="en">
        <Head>
          <meta charSet="utf-8" />
          <meta name="theme-color" content="#000000" />
          <link rel="shortcut icon" href="/img/favicon.png" />
          <link
            rel="apple-touch-icon"
            sizes="76x76"
            href="/img/apple-icon.png"
          />
          {/* Fonts and icons */}
          <link
            rel="stylesheet"
            type="text/css"
            href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons"
          />
          {/* HTML Meta Tags */}
          <meta name="description" content="Just a personal boring space" />

          {/* Facebook Meta Tags */}
          <meta property="og:url" content="https://cristianocasella.com" />
          <meta property="og:type" content="website" />
          <meta property="og:title" content="Cristiano Casella" />
          <meta property="og:description" content="Just a personal boring space" />
          <meta property="og:image" content="/img/caricature_sqr.jpg" />

          {/* Twitter Meta Tags */}
          <meta name="twitter:card" content="summary_large_image" />
          <meta property="twitter:domain" content="cristianocasella.com" />
          <meta property="twitter:url" content="https://cristianocasella.com" />
          <meta name="twitter:title" content="Cristiano Casella" />
          <meta name="twitter:description" content="Just a personal boring space" />
          <meta name="twitter:image" content="/img/caricature_sqr.jpg" />

          {/* Meta Tags Generated via https://www.opengraph.xyz */}

        </Head>
        <body>
          <div id="page-transition"></div>
          <Main />
          <NextScript />
          <script src="https://kit.fontawesome.com/ba0cc38827.js" crossOrigin="anonymous"></script>
        </body>
      </Html>
    );
  }
}

MyDocument.getInitialProps = async (ctx) => {
  // Resolution order
  //
  // On the server:
  // 1. app.getInitialProps
  // 2. page.getInitialProps
  // 3. document.getInitialProps
  // 4. app.render
  // 5. page.render
  // 6. document.render
  //
  // On the server with error:
  // 1. document.getInitialProps
  // 2. app.render
  // 3. page.render
  // 4. document.render
  //
  // On the client
  // 1. app.getInitialProps
  // 2. page.getInitialProps
  // 3. app.render
  // 4. page.render

  // Render app and page and get the context of the page with collected side effects.
  const sheets = new ServerStyleSheets();
  const originalRenderPage = ctx.renderPage;

  ctx.renderPage = () =>
    originalRenderPage({
      enhanceApp: (App) => (props) => sheets.collect(<App {...props} />),
    });

  const initialProps = await Document.getInitialProps(ctx);

  return {
    ...initialProps,
    // Styles fragment is rendered after the app and page rendering finish.
    styles: [
      <React.Fragment key="styles">
        {initialProps.styles}
        {sheets.getStyleElement()}
      </React.Fragment>,
    ],
  };
};

export default MyDocument;
