import { container, title } from "~/styles/jss/nextjs-material-kit.js";
import customCheckboxRadioSwitch from "~/styles/jss/nextjs-material-kit/customCheckboxRadioSwitch.js";

const basicsStyle = {
  sections: {
    padding: "70px 0",
  },
  container,
  title: {
    ...title,
    marginTop: "30px",
    minHeight: "32px",
    textDecoration: "none",
  },
  space50: {
    height: "50px",
    display: "block",
  },
  space70: {
    height: "70px",
    display: "block",
  },
  icons: {
    width: "17px",
    height: "17px",
    color: "#FFFFFF",
  },
  ...customCheckboxRadioSwitch,
  table: {
    border: "1px solid black",
    padding: "10px",
    textAlign: "center",
    marginLeft: "auto",
    marginRight: "auto"
  },
  th: {
    padding: "5px"
  },
  tr: {
    padding: "5px"
  },
  trfoot: {
    border: "0px",
  },
  td: {
    textAlign: "right",
    border: "1px solid darkgrey",
    padding: "5px"
  },
  tdfoot: {
    textAlign: "right",
    fontStyle: "italic",
    padding: "10px 5px 5px 5px"
  }
};

export default basicsStyle;
