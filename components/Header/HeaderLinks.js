/*eslint-disable*/
import React from "react";
import Link from "next/link";

// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import Tooltip from "@material-ui/core/Tooltip";
import Icon from "@material-ui/core/Icon";

// @mui/icons-material
import { Apps, CloudDownload } from "@mui/icons-material";
import DeleteIcon from "@mui/icons-material/Delete";
import IconButton from "@material-ui/core/IconButton";

// core components
import CustomDropdown from "~/components/CustomDropdown/CustomDropdown.js";
import Button from "~/components/CustomButtons/Button.js";

import styles from "~/styles/jss/nextjs-material-kit/components/headerLinksStyle.js";

const useStyles = makeStyles(styles);

export default function HeaderLinks(props) {
  const classes = useStyles();
  return (
    (<List className={classes.list}>
      <ListItem className={classes.listItem}>
        <Link href="/ambiente" legacyBehavior>
        <Button
          color="transparent"
          target="_blank"
          className={classes.navLink}
        >
          <Icon className={classes.icons}>park</Icon> Ambiente
        </Button>
        </Link>
      </ListItem>
      <ListItem className={classes.listItem}>
        <Link href="/posts" legacyBehavior>
        <Button
          color="transparent"
          target="_blank"
          className={classes.navLink}
        >
          <Icon className={classes.icons}>layers</Icon> Posts
        </Button>
        </Link>
      </ListItem>
      <ListItem className={classes.listItem}>
        <Link href="/talks" legacyBehavior>
        <Button
          color="transparent"
          target="_blank"
          className={classes.navLink}
        >
          <Icon className={classes.icons}>campaign</Icon> Talks
        </Button>
        </Link>
      </ListItem>
      <ListItem className={classes.listItem}>
        <Link href="/covid" legacyBehavior>
        <Button
          color="transparent"
          target="_blank"
          className={classes.navLink}
        >
          <Icon className={classes.icons}>coronavirus</Icon> Covid-19
        </Button>
        </Link>
      </ListItem>
    </List>)
  );
}
