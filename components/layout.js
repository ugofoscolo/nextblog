import Head from 'next/head'
import Image from "next/legacy/image"
import styles from './layout.module.css'
import utilStyles from '../styles/utils.module.css'
import Link from 'next/link'

const name = 'Cristiano'
export const siteTitle = 'Cristiano\'s Blog'

export default function Layout({ children, home }) {
  return (
    (<div className={styles.container}>
      <Head>
        <link rel="icon" href="/img/favicon.png" />
        <meta
          name="description"
          content="Just a boring personal space about me."
        />
        <meta
          property="og:image"
          content="/img/caricature_sqr.jpg"
        />
        <meta name="og:title" content={siteTitle} />
        <meta name="og:type" content="website" />
        <meta name="twitter:card" content="summary_large_image" />
      </Head>
      <header className={styles.header}>
        {home ? (
          <>
            <Image
              priority
              src="/img/caricature_sqr.jpg"
              className={utilStyles.borderCircle}
              height={144}
              width={144}
              alt={name}
            />
            <h1 className={utilStyles.heading2Xl}>{name}</h1>
          </>
        ) : (
          <>
            <Link href="/">

              <Image
                priority
                src="/img/caricature_sqr.jpg"
                className={utilStyles.borderCircle}
                height={108}
                width={108}
                alt={name}
              />

            </Link>
            <h2 className={utilStyles.headingLg}>
              <Link href="/" className={utilStyles.colorInherit}>
                {name}
              </Link>
            </h2>
          </>
        )}
      </header>
      <main>{children}</main>
      {!home && (
        <div className={styles.backToHome}>
          <Link href="/">
            ← Back to home
          </Link>
        </div>
      )}
    </div>)
  );
}
