---
title: 'GitLab pipeline: pubblicare su FTP'
layout: post
date: '2020-09-06'
tags:
- gitlab
- pipeline
- ftp
categories:
- tutorial
---

In questo articolo vediamo come creare un job di GitLab per pubblicare su una destinazione FTP dalla nostra pipeline di CI/CD.   <!--more-->

Questo &eacute; lo snippet che ho utilizzato:
```
production:
  stage: production
  only:
    - master
  script:
    - apk update && apk add lftp
    - lftp -c "set ftp:ssl-allow no; open -u $FTPUSER,$FTPPASS $FTPHOST; mirror -Rev $SOURCE $DESTINATION --ignore-time --parallel=10 --exclude-glob .git* --exclude .git/"
  when:
    manual
  environment:
    name: production
    url: https://miositoweb.it
```
L'immagine di partenza in questo caso &eacute; una *Alpine*, nel caso utilizziate una immagine diversa va adattata la parte di installazione di *LFTP*.
Le opzioni che vengono passate in questo caso sono:
- la comunicazione in plain ftp
- utente, password ed host ftp che sono memorizzate nella variabile di ambiente della pipeline (in questo modo posso differenziarli in basse all'ambiente)
- la parte di sync delle directory di sorgente e destinazione (va ovviamente sostituito $SOURCE e $DESTINATION), compresa la parte di rimozione remota di file non pi&uacute; presenti.
- una lista di directory da escludere, come la cartella di *git*.
