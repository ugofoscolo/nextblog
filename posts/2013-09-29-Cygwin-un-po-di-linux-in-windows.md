---
title: Cygwin - Un po' di Linux in Windows
layout: post
date: '2013-09-29'
tags:
- linux
- windows
- os
- shell
categories:
- tutorial
---

<h2>Introduzione a Cygwin</h2>

Per chi è abituato ad utilizzare una qualsiasi distribuzione di Linux, una delle difficoltà che si incontrano spesso passando a Windows o a Mac, è una forte mancanza di tutta una serie di strumenti da riga di comando fondamentali per l’amministrazione del computer o server locale e/o remoto, di una rete più o meno complessa e di tutte quelle operazioni necessarie per chi opera nell’IT.

Microsoft con la PowerShell ha cercato di colmare questo gap, fornendo una shell di amministrazione scalabile ed estendibile, il problema è che è molto orientata verso i propri prodotti e non offre strumenti più generici.

Per le mie personali esigenze ho trovato in [Cygwin](http://cygwin.com/) la risposta<!--more-->:

Questo programma permette l’installazione tramite un repository dei più comuni tools presenti in una shell linux, permette anche di installare un X Server, supporta tranquillamente le più recenti versioni di Windows (io la sto usando attualmente su Windows 8) ed è molto semplice da utilizzare.

<h2>Installazione</h2>

A questo link è possibile scaricare l’installer, oltre che per la prima installazione questo si renderà necessario ogni qual volta vorremo aggiornare i pacchetti, installarne di nuovi o rimuoverli, contiene un vero e proprio packages manager.

Durante l’installazione viene chiesta ovviamente la cartella di installazione del programma ed una cartella da destinare al repository, attenzione che di default selezionerà la cartella dal quale è stato lanciato l’installer, io preferisco creare una cartella all’interno di quella di installazione, così da avere tutto ordinato.

Una volta selezionato il mirror sarà possibile scegliere quali pacchetti installare e se includere eventualmente i sorgenti.

Nel mio caso i pacchetti fondamentali sono i più comuni per la rete (whois, ssh client e server, telnet….), editor di testo (vim ed affini) e gestori SVN (svn, git…etc).

<h2>Integrazione con Windows</h2>

Una volta installato Cygwin ed i pacchetti da noi scelti verrà creato un vero e proprio sotto sistema unix, aprendo la shell ci troveremo in una struttura di directory tipica di una qualsiasi distro di linux.

Elenco cartelle:

- bin
- cygdrive
- dev
- etc
- home
- lib
- packages
- proc
- tmp
- usr
- var

L’unica cartella *anomala* è cygdrive, qui dentro troviamo tutti i nostri dischi e le varie unità montate su Windows, potremo quindi da qui accedere ai nostri files.

Come si può notare durante la fase di installazione i programmi che installiamo tramite cygwin sono dei files eseguibili da Windows normalmente, potremo quindi integrarli anche con il normale prompt di ms-dos aggiungendo la cartella bin di Cygwin alle variabili d’ambiente di Windows.

Per fare questo cliccate con il tasto destro su Risorse del computer, Proprietà, Avanzate, Variabili d’ambiente, nella parte bassa cliccate su Path tra le Variabili di Sistema e poi su Modifica, in fondo alla riga mettete un ; dopo l’ultimo percorso ed aggiungete poi quello della vostra installazione, es: `C:\cygwin64\bin\`.

Da questo momento potrete usare i tools installati con Cygwin anche dal prompt di ms-dos.

<h2>Personalizzazione</h2>

Esattamente come su Linux potete personalizzare il vostro ambiente editando il file presente nella vostra home (quella dell’ambiente unix, quindi `/home/$nome_utente`, non `/cygdrive/c/Users/$nome_utente` che è di windows) `.bashrc` e `.bash_profile`, nel mio caso ho personalizzato anche i file presenti in `.vim` e `.ssh` per personalizzare il mio editor di testo e le opzioni relative alle connessioni ssh verso altri server.
