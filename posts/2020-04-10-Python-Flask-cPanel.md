---
title: Python, Flask e cPanel
layout: post
date: '2020-04-10'
tags:
- python
- flask
- cpanel
categories:
- tutorial
---

<b>Python</b> &egrave; sicuramente il mio linguaggio di programmazione preferito, ordinato, potente, versatile, riesce sempre a soddisfare facilmente le mie esigenze di coding. Un modulo molto utile per l'implementazione di microservizi e webapp &egrave; [Flask](https://github.com/pallets/flask).<!--more-->

<i><b>Flask</b> is a lightweight <b>WSGI</b> web application framework. It is designed to make getting started quick and easy, with the ability to scale up to complex applications. It began as a simple wrapper around Werkzeug and Jinja and has become one of the most popular Python web application frameworks.</i>

<i>Flask offers suggestions, but doesn't enforce any dependencies or project layout. It is up to the developer to choose the tools and libraries they want to use. There are many extensions provided by the community that make adding new functionality easy."</i>

Questa accoppiata si presta benissimo alla creazione di applicazioni basate su Docker e microservizi ma a volte questo &egrave; esagerato per progetti più semplici, l'utilizzo di un <b>hosting</b> tradizionale permette invece di mantenere l'<b>infrastruttura semplice</b> ed i <b>costi contenuti</b>.

La soluzione per la mia ultima app che ho trovato per risolvere tutto questo &egrave; l'hosting su piattaforma <b>cPanel</b>.

Il punto di partenza &egrave; avere ovviamente la propria app funzionante, la versione di cPanel da me utilizzata &egrave; la `78.0.39`, consiglio un hosting con <b>accesso in SSH</b>, rende le cose estremamente pi&ugrave; semplici, soprattutto in caso di debug, consiglio inoltre di caricare i propri file DOPO aver impostato l'ambiente di Python.

Da cPanel selezionate <b>Setup Python App</b>, successivamente <b>Create Application</b>.

I parametri richiesti a questo punto sono:

<b>Python Version</b>: Dal menu a tendina potete scegliere la versione desiderata.
<b>Application Root</b>: Il percorso di installazione dell'applicazione, se viene selezionato un percorso non esistente verr&agrave; creato automaticamente il necessario. Consiglio di scegliere una directory fuori da public_html.
<b>Application URL</b>: Il percorso al quale l'applicazione risponder&agrave;.
<b>Application Startup File</b>: Il nome del file che permette l'esecuzione della vostra app, su Flask &egrace; il file che di solito contiene o finisce con `app.run()` (I nomi pi&ugrave; comuni sono <i>app.py</i>, <i>main.py</i>, <i>index.py</i>, o <i>init.py</i>).
<b>Application Entry Point</b>: Nel file di Startup cercate l'importazione iniziale, di solito &egrave; tipo `from app import app` o `from app import application` o simile, qui va inserito il nome del modulo come viene importato (l'ultima parola della riga), tipicamente <i>app</i> o <i>application</i> come nell'esempio appena citato.

A questo punto si pu&ograve; confermare la creazione dell'applicazione dal tasto in alto a destra.
WSGI si preoccuper&agrave; di reindirizzare le chiamate per la URL definita verso Flask.
Vi consiglio di provare a visitare il sito web, dovreste vedere una pagina web confermante il funzionamento dell'applicazione.

Adesso potete caricare nella cartella creata automaticamente i vostri file, rispettando ovviamente le dichiarazioni di cui sopra.
Tornando nella configurazione dell'applicazione in cPanel avrete la possibilit&agrave; di:

<ul>
<li>Fermare/Avviare/Riavvare l'applicazione.</li>
<li>Selezionare il file <i>requirements.txt</i> per poter installare i moduli necessari.</li>
<li>Impostare le variabili di ambiente nel processo.</li>
</ul>

Ogni volta che viene modificata l'applicazione sarà necessario riavviarla.
