---
title: Configurare Windows per l’utilizzo di UTC nel Bios RTC
layout: post
date: '2014-11-22'
tags:
- windows
- linux
- utc
- bios
- time
categories:
- tutorial
---

<h2>Differenze di orario</h2>

Per chi condivide l’hard disk tra Windows ed un altro sistema operativo (ad esempio Linux) uno dei possibili imprevisti possibili è la gestione dell’orologio del Bios RTC (real time clock). Il sistema operativo di Redmond infatti non supporta nativamente UTC ed utilizza sempre l’orario localizzato. Ad ogni avvio del computer e/o ogni modifica dell’orologio scriverà il proprio orario nel bios. Alcuni sistemi operativi Unix based e non mal gradiscono questo tipo di configurazione e vorrebbero che l’orologio di sistema fosse impostato in UTC, lasciando alla parte utente la localizzazione con il relativo fuso orario. Il classico risultato di questa differenza di opinioni tra i due sistemi operativi sarà che windows metterà l’ora locale nel bios, e linux, leggendo tale orario aggiungerà ad un orario già localizzato la differenza di fuso orario, ottenendo quindi un’ora completamente errata. Per ovviare a questo problema senza fare dispetto al sistema unix based la soluzione sta nel fare questa piccola modifica a Windows. <!--more-->

<h2>Primo metodo: Creazione file di registro</h2>

Il metodo più veloce è creare un file di testo vuoto `utc.reg` ed inserirvi il seguente contenuto:

{% highlight shell %}
Windows Registry Editor Version 5.0
[HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\TimeZoneInformation] “RealTimeIsUniversal”=dword:00000001
{% endhighlight %}

Eseguirlo e poi riavviare il computer!

<h2>Secondo metodo: Modifica manuale registro di sistema</h2>

Il metodo manuale prevede invece l’esecuzione dell’editor del Registro di Sistema per eseguire a mano la modifica.

Selezionare Start > Esegui > digitare `regedit` e premere invio

Selezionare dal menu a sinistra il seguente percorso:

`HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\TimeZoneInformation`

Creare una nuova <b>dword</b> sulla destra, assegnargli il nome `RealTimeIsUniversal` ed impostare come valore `1`

Riavviare il computer!
