---
title: Padroneggiare il continuous software development
layout: talk
date: '2020-06-23'
tags:
- CI
- CD
- software
- development
- gitlab
categories:
- talks
---

Non permettere ai tuoi tools di rallentarti. Approcciando il continuous deployment all’interno del tuo ciclo produttivo, è fondamentale avere la giusta applicazione di supporto.

I sistemi di CI/CD basati su plugins come Jenkins non sono scalabili, aumentano la complessità del tuo ciclo produttivo ed aggiungono possibili failure points al processo. Quando i tuoi strumenti di CI/CD sono già forniti pronti all’uso la manutenzione e la gestione sono semplificati, permettendoti di essere produttivo più velocemente.

Scopri di più sui software di CI/CD e su come GitLab fornisce tutto questo già pronto all’uso aiutandoti ad aumentare la velocità del tuo ciclo sviluppo software del 200%.

Nel webcast, parleremo di:
- I tre principali approcci al metodo di continuous software development
- I benefici di Continuous Integration, Delivery e Deployment
- Dimostrazione delle pipeline di CI/CD di GitLab per build, test, deploy, e monitoraggio del tuo codice.

<a href="https://about.gitlab.com/webcast/mastering-ci-cd-italian/" target="_blank">Puoi vedere la registrazione del webcast qui</a>
