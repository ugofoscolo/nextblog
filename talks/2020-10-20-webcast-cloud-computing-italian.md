---
title: 'Cloud Native Transformation: il mondo sta cambiando'
layout: talk
date: '2020-10-20'
tags:
- Cloud Native
categories:
- talks
---
La trasformazione digitale &egrave; una pratica in continua evoluzione. Non appena emerge una nuova tecnologia digitale, le aziende enterprise devono adattarsi. Per alcuni, questo significa continuare e scalare una trasformazione esistente. Per altri &egrave; l'opportunità di balzare nell'era moderna, adottando tecnologie Cloud Native.

In questo video scoprirai...
* Perch&eacute; le aziende enterprise si stanno muovendo verso il Cloud Native
* Le sfide dell'approccio Cloud Native
* Come le aziende di successo stanno affrontando il cambiamento
* La visione Cloud Native di GitLab

<a href="https://page.gitlab.com/webcast-cloud-computing-italian.html" target="_blank">Puoi vedere la registrazione del webcast qui</a>
