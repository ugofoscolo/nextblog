---
title: Linux-Lab 2017 - Dall'applicativo monolitico alla mesosfera
layout: talk
date: '2017-12-07'
tags:
- microservizi
- container
- Mesosphere
- Mesos
- Marathon
categories:
- talks
---

Come convertire una infrastruttura web tradizionale con apache/nginx e database in una infrastruttura basata su container e <b>microservizi</b> su piattaforma <b>Mesosphere</b>.<br />
Nel talk viene descritto un ipotetico processo di migrazione dalle vecchie piattaforme ospitanti applicativi web interni e siti (tipicamente basate su apache/nginx ed un database) verso una piattaforma basata su container, nello specifico <b>Marathon</b> + <b>Mesos</b>.<br />
Viene affrontato il talk da un punto di vista strutturale, andando ad analizzare la vecchia e soprattutto la nuova piattaforma, mostrando anche le possibilità di evoluzione degli applicativi su una piattaforma di questo tipo, orientata quindi ai <b>microservizi</b> e le API.<br /><br />
La pagina dell'evento è disponibile <a href="https://2017.linux-lab.it/talks/dallapplicativo_monolitico_alla_mesosfera_2017-12-07/" target="_blank">qui</a>.
