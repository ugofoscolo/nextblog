---
title: 'Dal codice alla produzione in un solo strumento'
layout: talk
date: '2021-01-26'
tags:
- SDLC
- GitLab
- DevOps
categories:
- talks
---
Un piccolo viaggio attraverso le diverse fasi di un ciclo di sviluppo software moderno. Vediamo come possiamo gestire l'intero processo di sviluppo in un solo strumento, ottimizzando costi e tempi nel nostro flusso di lavoro. Partendo da un clone git per una semplice applicazione web, useremo GitLab nelle diverse fasi per modificare, testare, distribuire, monitorare e difendere la nostra base di codice in un ambiente nativo cloud. L'interfaccia utente raccoglierà e mostrerà facilmente tutte le informazioni di sicurezza e di qualità del nostro software, ottimizzando la gestione delle minacce.

<a href="https://events.codemotion.com/webinars/dal-codice-alla-produzione-in-un-solo-strumento/" target="_blank">Registrati all'evento gratuitamente</a>
