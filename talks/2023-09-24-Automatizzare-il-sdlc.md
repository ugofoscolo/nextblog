---
title: 'Come to Code 2023 - Automatizzare il Software Development Lifecycle'
layout: talk
date: '2023-09-24'
tags:
- GitLab
- Conventional Commit
- CI/CD
- Semantic Release
- Renovate Bot
categories:
- talks
---

Con l'aiuto di GitLab ed altri strumenti OpenSource vedremo come automatizzare il SDLC e quali sono i principali vantaggi di questo approccio. Utilizzando un progetto demo cercheremo di automatizzarne i controlli di qualità e di sicurezza per poi arrivare alla build ed al deployment del progetto.

La pagina dell' evento è disponibile <a href="https://www.cometocode.it/agenda-dettaglio.html#content4-5j" target="_blank">qui</a>.

Il video dell' evento è disponibile <a href ="https://youtu.be/n_ffqODwwrI" target="_blank">qui</a>.
