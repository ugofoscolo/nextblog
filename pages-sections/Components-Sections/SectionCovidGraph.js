import React from 'react';
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  LogarithmicScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend,
  Filler,
} from "chart.js";
ChartJS.register(
  CategoryScale,
  LinearScale,
  LogarithmicScale,
  PointElement,
  LineElement,
  Filler,
  Title,
  Tooltip,
  Legend
);
import { Line } from "react-chartjs-2";
import json from '~/json/covid.json';
import { format } from "date-fns";

export function GraphNcasi() {
  const dataset1 = json.map(function(e){ return e.totale_positivi})
  const dataset2 = json.map(function(e){ return e.nuovi_positivi})
  const dataset3 = json.map(function(e){
    const perc = 100 / e.totale_positivi * e.nuovi_positivi
    return perc
  })
  const labelset = json.map(function(e){ return fDate(e.data)})
  const data = {
    labels: labelset,
    datasets: [
      {
        label: 'Totale Positivi',
        fill: false,
        lineTension: 0.1,
        backgroundColor: 'blue',
        borderColor: 'blue',
        borderCapStyle: 'butt',
        borderDash: [],
        borderDashOffset: 0.0,
        borderJoinStyle: 'miter',
        pointBorderColor: 'blue',
        pointBackgroundColor: '#fff',
        pointBorderWidth: 1,
        pointHoverRadius: 5,
        pointHoverBackgroundColor: 'blue',
        pointHoverBorderColor: 'blue',
        pointHoverBorderWidth: 2,
        pointRadius: 1,
        pointHitRadius: 10,
        data: dataset1,
        yAxisID: 'first-y-axis'
      },
      {
        label: 'Nuovi Positivi',
        fill: false,
        lineTension: 0.1,
        backgroundColor: 'red',
        borderColor: 'red',
        borderCapStyle: 'butt',
        borderDash: [],
        borderDashOffset: 0.0,
        borderJoinStyle: 'miter',
        pointBorderColor: 'red',
        pointBackgroundColor: '#fff',
        pointBorderWidth: 1,
        pointHoverRadius: 5,
        pointHoverBackgroundColor: 'red',
        pointHoverBorderColor: 'red',
        pointHoverBorderWidth: 2,
        pointRadius: 1,
        pointHitRadius: 10,
        data: dataset2,
        yAxisID: 'first-y-axis'
      },
      {
        label: '% Nuovi Positivi',
        fill: false,
        lineTension: 0.1,
        backgroundColor: 'orange',
        borderColor: 'orange',
        borderCapStyle: 'butt',
        borderDash: [],
        borderDashOffset: 5.0,
        borderJoinStyle: 'miter',
        pointBorderColor: 'orange',
        pointBackgroundColor: '#fff',
        pointBorderWidth: 0.5,
        pointHoverRadius: 5,
        pointHoverBackgroundColor: 'orange',
        pointHoverBorderColor: 'orange',
        pointHoverBorderWidth: 2,
        pointRadius: 0.5,
        pointHitRadius: 10,
        data: dataset3,
        yAxisID: 'second-y-axis'
      }
    ]
  }
  const options = {
    scales: {
      'first-y-axis': {
        type: 'logarithmic',
        position: 'left',
        title: {
          display: true,
          text: 'Numero Positivi'
        }
      },
      'second-y-axis': {
        position: 'right',
        title: {
          display: true,
          text: 'Percentuale Nuovi Positivi'
        }
      }
    }
  }

  return (
      <div>
        <h3>Nuovi Positivi</h3>
          <Line
          data={data}
          options={options}
          width={400}
          height={250}
          />
      </div>
  );
}

export function GraphLog() {
  const dataset = json
  const data = {
    datasets: [
      {
        label: 'Nuovi Positivi',
        fill: false,
        lineTension: 0.1,
        backgroundColor: 'blue',
        borderColor: 'blue',
        borderCapStyle: 'butt',
        borderDash: [],
        borderDashOffset: 0.0,
        borderJoinStyle: 'miter',
        pointBorderColor: 'blue',
        pointBackgroundColor: '#fff',
        pointBorderWidth: 1,
        pointHoverRadius: 5,
        pointHoverBackgroundColor: 'blue',
        pointHoverBorderColor: 'blue',
        pointHoverBorderWidth: 2,
        pointRadius: 1,
        pointHitRadius: 10,
        data: dataset,
        xAxisID: 'test',
        yAxisID: 'first-y-axis'
      }
    ]
  }
  const options = {
    parsing: {
      yAxisKey: 'nuovi_positivi',
      xAxisKey: 'totale_positivi'
    },
    scales: {
      'test': {
        type: 'logarithmic',
        position: 'bottom',
        display: true,
        title: {
          display: true,
          text: 'Totale Casi'
        }
      },
      'first-y-axis': {
        position: 'left',
        type: 'logarithmic',
        title: {
          display: true,
          text: 'Nuovi Casi'
        }
      }
    }
  }

  return (
      <div>
        <h3>Andamento Logaritmico dei Nuovi Casi</h3>
          <Line
          data={data}
          options={options}
          width={400}
          height={250}
          />
      </div>
  );
}

export function GraphTest() {
  const dataset1 = json.map(function(e){ return e.totale_casi})
  const dataset2 = json.map(function(e){ return e.tamponi})
  const dataset3 = json.map(function(e){
    const perc = 100 / e.tamponi * e.totale_casi
    return perc
  })
  const labelset = json.map(function(e){ return fDate(e.data)})
  const data = {
    labels: labelset,
    datasets: [
      {
        label: 'Totale Casi',
        fill: false,
        lineTension: 0.1,
        backgroundColor: 'blue',
        borderColor: 'blue',
        borderCapStyle: 'butt',
        borderDash: [],
        borderDashOffset: 0.0,
        borderJoinStyle: 'miter',
        pointBorderColor: 'blue',
        pointBackgroundColor: '#fff',
        pointBorderWidth: 1,
        pointHoverRadius: 5,
        pointHoverBackgroundColor: 'blue',
        pointHoverBorderColor: 'blue',
        pointHoverBorderWidth: 2,
        pointRadius: 1,
        pointHitRadius: 10,
        data: dataset1,
        yAxisID: 'first-y-axis'
      },
      {
        label: 'Tamponi',
        fill: false,
        lineTension: 0.1,
        backgroundColor: 'red',
        borderColor: 'red',
        borderCapStyle: 'butt',
        borderDash: [],
        borderDashOffset: 0.0,
        borderJoinStyle: 'miter',
        pointBorderColor: 'red',
        pointBackgroundColor: '#fff',
        pointBorderWidth: 1,
        pointHoverRadius: 5,
        pointHoverBackgroundColor: 'red',
        pointHoverBorderColor: 'red',
        pointHoverBorderWidth: 2,
        pointRadius: 1,
        pointHitRadius: 10,
        data: dataset2,
        yAxisID: 'first-y-axis'
      },
      {
        label: '% Tamponi Positivi',
        fill: false,
        lineTension: 0.1,
        backgroundColor: 'orange',
        borderColor: 'orange',
        borderCapStyle: 'butt',
        borderDash: [],
        borderDashOffset: 5.0,
        borderJoinStyle: 'miter',
        pointBorderColor: 'orange',
        pointBackgroundColor: '#fff',
        pointBorderWidth: 0.5,
        pointHoverRadius: 5,
        pointHoverBackgroundColor: 'orange',
        pointHoverBorderColor: 'orange',
        pointHoverBorderWidth: 2,
        pointRadius: 0.5,
        pointHitRadius: 10,
        data: dataset3,
        yAxisID: 'second-y-axis'
      }
    ]
  }
  const options = {
    scales: {
      'first-y-axis': {
        type: 'logarithmic',
        position: 'left',
        title: {
          display: true,
          text: 'Totale Casi/Tamponi'
        }
      },
      'second-y-axis': {
        position: 'right',
        title: {
          display: true,
          text: 'Percentuale Tamponi Positivi'
        }
      }
    }
  }

  return (
    <div>
      <h3>Nuovi Positivi</h3>
        <Line
        data={data}
        options={options}
        width={400}
        height={250}
        />
    </div>
);
}

export function fDate(date) {
  return (
    format(new Date(date), 'yyyy-MM-dd')
  )
}
