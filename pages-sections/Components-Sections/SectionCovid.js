import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import styles from "../../styles/jss/nextjs-material-kit/pages/componentsSections/basicsStyle.js";
const useStyles = makeStyles(styles);

import json from '~/json/covid.json'
import { format } from "date-fns";
import { NumericFormat } from 'react-number-format';

export function UltimiDati() {
  const last = json.slice().reverse()[0]
  const prelast = json.slice().reverse()[1]
  const classes = useStyles();
  const restday =  ((last.totale_positivi / last.variazione_totale_positivi * -1 )+ 1).toString().split('.', '1')
  const ending = 'A questo ritmo per debellare il virus servono circa: ' + restday + ' giorni'
 return (
   (<div className={classes.sections}>
     <div className={classes.container}>
       <table className={(classes.ultimidati, classes.table)}>
         <thead>
           <tr className={classes.trhead}>
             <th className={classes.th}>&Delta; Casi Attivi</th>
             <th className={classes.th}>&Delta; Totale Casi</th>
             <th className={classes.th}>Nuove Guarigioni</th>
             <th className={classes.th}>Nuovi Decessi</th>
           </tr>
         </thead>
         <tbody>
           <tr className={classes.tr}>
             <td className={classes.td}>{ fNumber(last.variazione_totale_positivi) }</td>
             <td className={classes.td}>{ fNumber(last.nuovi_positivi) }</td>
             <td className={classes.td}>{ fNumber(last.dimessi_guariti - prelast.dimessi_guariti) }</td>
             <td className={classes.td}>{ fNumber(last.deceduti - prelast.deceduti) }</td>
           </tr>
         </tbody>
         <tfoot>
           <tr className={classes.trfoot}>
             <td colSpan="4" className={classes.tdfoot}>Dati Aggiornati al: { fDate(last.data)}<br />{ ending }</td>
           </tr>
         </tfoot>
       </table>
     </div>
   </div>)
 );
}

export function PercentualiGenerali() {
  const last = json.slice().reverse()[0]
  const prelast = json.slice().reverse()[1]
  const classes = useStyles();
  return (
    (<div className={classes.sections}>
      <div className={classes.container}>
        <table className={(classes.percentualigenerali, classes.table)}>
          <thead>
            <tr className={classes.trhead}>
              <th className={classes.th}>Percentuale Guariti</th>
              <th className={classes.th}>Percentuale Deceduti</th>
              <th className={classes.th}>Percentuale Isolamento Domiciliare *</th>
              <th className={classes.th}>Percentuale Ricoverati *</th>
              <th className={classes.th}>Percentuale Terapia Intensiva *</th>
            </tr>
          </thead>
          <tbody>
            <tr className={classes.tr}>
              <td className={classes.td}>{ fPerc(100 * last.dimessi_guariti / last.totale_casi) }</td>
              <td className={classes.td}>{ fPerc(100 * last.deceduti / last.totale_casi) }</td>
              <td className={classes.td}>{ fPerc(100 * last.isolamento_domiciliare / last.totale_positivi) }</td>
              <td className={classes.td}>{ fPerc(100 * last.totale_ospedalizzati / last.totale_positivi) }</td>
              <td className={classes.td}>{ fPerc(100 * last.terapia_intensiva / last.totale_positivi) }</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>)
  );
}

export function NuoviDati() {
  const classes = useStyles();
  return (
    (<div className={classes.sections}>
      <div className={classes.container}>
        <table className={(classes.percentualigenerali, classes.table)}>
          <thead>
            <tr className={classes.trhead}>
              <th className={classes.th}>Data</th>
              <th className={classes.th}>Nuovi Casi</th>
              <th className={classes.th}>Percentuale Nuovi Casi</th>
              <th className={classes.th}>Casi Attualmente Attivi</th>
            </tr>
          </thead>
          <tbody>
            {json.reverse().slice(0,10).map(({ data, nuovi_positivi, totale_positivi }) => (
            <tr className={classes.tr} key={data}>
              <td className={classes.td}>{ fDate(data) }</td>
              <td className={classes.td}>{ fNumber(nuovi_positivi) }</td>
              <td className={classes.td}>{ fPerc(100 / totale_positivi * nuovi_positivi) }</td>
              <td className={classes.td}>{ fNumber(totale_positivi) }</td>
            </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>)
  );
}

export function TestCovid() {
  const classes = useStyles();
  return (
    (<div className={classes.sections}>
      <div className={classes.container}>
        <table className={(classes.percentualigenerali, classes.table)}>
          <thead>
            <tr className={classes.trhead}>
              <th className={classes.th}>Data</th>
              <th className={classes.th}>Positivi Test Molecolare</th>
              <th className={classes.th}>Totale Test Molecolare</th>
              <th className={classes.th}>Percentuale Positivi Test Molecolare</th>
              <th className={classes.th}>Positivi Test Antigenico Rapido</th>
              <th className={classes.th}>Totale Test Antigenico Rapido</th>
              <th className={classes.th}>Percentuale Positivi Test Antigenico Rapido</th>
              <th className={classes.th}>Totale Positivi</th>
              <th className={classes.th}>Percentuale Positivi Generale</th>
            </tr>
          </thead>
          <tbody>
            {json.slice(0,10).map(({ data, totale_positivi_test_molecolare, tamponi_test_molecolare, totale_positivi_test_antigenico_rapido, tamponi_test_antigenico_rapido, tamponi, totale_casi }) => (
            <tr className={classes.tr} key={data}>
              <td className={classes.td}>{ fDate(data) }</td>
              <td className={classes.td}>{ fNumber(totale_positivi_test_molecolare) }</td>
              <td className={classes.td}>{ fNumber(tamponi_test_molecolare) }</td>
              <td className={classes.td}>{ fPerc(100 / tamponi_test_molecolare * totale_positivi_test_molecolare) }</td>
              <td className={classes.td}>{ fNumber( totale_positivi_test_antigenico_rapido) }</td>
              <td className={classes.td}>{ fNumber(tamponi_test_antigenico_rapido) }</td>
              <td className={classes.td}>{ fPerc(100 / tamponi_test_antigenico_rapido * totale_positivi_test_antigenico_rapido) }</td>
              <td className={classes.td}>{ fNumber(totale_casi) }</td>
              <td className={classes.td}>{ fPerc(100 / tamponi * totale_casi) }</td>
            </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>)
  );
}

export function GuaritiDeceduti() {
  const classes = useStyles();
  return (
    (<div className={classes.sections}>
      <div className={classes.container}>
        <table className={(classes.percentualigenerali, classes.table)}>
          <thead>
            <tr className={classes.trhead}>
              <th className={classes.th}>Data</th>
              <th className={classes.th}>Dimessi Guariti</th>
              <th className={classes.th}>Percentuale Guariti</th>
              <th className={classes.th}>Deceduti</th>
              <th className={classes.th}>Percentuale Deceduti</th>
              <th className={classes.th}>Totale Casi</th>
            </tr>
          </thead>
          <tbody>
            {json.slice(0,10).map(({ data, dimessi_guariti, totale_casi, deceduti }) => (
            <tr className={classes.tr} key={data}>
              <td className={classes.td}>{ fDate(data) }</td>
              <td className={classes.td}>{ fNumber(dimessi_guariti) }</td>
              <td className={classes.td}>{ fPerc(100 / totale_casi * dimessi_guariti) }</td>
              <td className={classes.td}>{ fNumber(deceduti) }</td>
              <td className={classes.td}>{ fPerc(100 / totale_casi * deceduti ) }</td>
              <td className={classes.td}>{ fNumber(totale_casi) }</td>
            </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>)
  );
}

export function SituazioneRicoveri() {
  const classes = useStyles();
  return (
    (<div className={classes.sections}>
      <div className={classes.container}>
        <table className={(classes.percentualigenerali, classes.table)}>
          <thead>
            <tr className={classes.trhead}>
              <th className={classes.th}>Data</th>
              <th className={classes.th}>Ricoverati con Sintomi</th>
              <th className={classes.th}>Terapia Intensiva</th>
              <th className={classes.th}>Isolamento Domiciliare</th>
              <th className={classes.th}>Casi Attualmente Positivi</th>
            </tr>
          </thead>
          <tbody>
            {json.slice(0,10).map(({ data, ricoverati_con_sintomi, terapia_intensiva, isolamento_domiciliare, totale_positivi}) => (
            <tr className={classes.tr} key={data}>
              <td className={classes.td}>{ fDate(data) }</td>
              <td className={classes.td}>{ fNumber(ricoverati_con_sintomi) }</td>
              <td className={classes.td}>{ fNumber(terapia_intensiva) }</td>
              <td className={classes.td}>{ fNumber(isolamento_domiciliare) }</td>
              <td className={classes.td}>{ fNumber(totale_positivi) }</td>
            </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>)
  );
}

export function fNumber(num) {
  return (
    <NumericFormat value={ num } thousandSeparator="." displayType={'text'} decimalScale={'0'} decimalSeparator="," />
  )
}

export function fPerc(num) {
  return (
    <NumericFormat value={ num } thousandSeparator="." displayType={'text'} decimalScale={ '2' } decimalSeparator="," suffix={ '%' }/>
  )
}

export function fDate(date) {
  return (
    format(new Date(date), 'yyyy-MM-dd')
  )
}
