import React from "react";
// plugin that creates slider
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
// @mui/icons-material
import Favorite from "@mui/icons-material/Favorite";
import People from "@mui/icons-material/People";
import Check from "@mui/icons-material/Check";
import FiberManualRecord from "@mui/icons-material/FiberManualRecord";
// core components
import styles from "../../styles/jss/nextjs-material-kit/pages/componentsSections/basicsStyle.js";

const useStyles = makeStyles(styles);

export default function SectionWishlist() {
  const classes = useStyles();
  return (
    <div className={classes.sections}>
      <div className={classes.container}>
        <p>A quanto pare sono una persona piuttosto complicata alla quale fare regali, per questo motivo dopo svariate richieste ho deciso di creare (e mi impegno a mantenere nel tempo) una wishlist per aiutare il malcapitato di turno. Per eventuali dubbi o altro rompete pure le scatole alla mia dolce metá.</p>

        <h2>Sempre verdi</h2>
        <ul>
          <li><i className="fas fa-utensils"></i> Accessori cucina</li>
          <li><i className="fas fa-guitar"></i> Accessori chitarra (suggerimenti da <a href="https://www.youtube.com/watch?v=uJ0I_wvOas0" target="_blank">GasTube</a>)</li>
          <li><i className="fas fa-wine-bottle"></i> Whiskey (torbati tipo scozzese o giapponesi), rhum agricoli, birra artigianale (luppolate in genere, ipa, porter e stout, meglio se imperial)</li>
          <li><i className="fas fa-user-tie"></i> Buono per taglio/rasatura <a href="https://bluesbarber.it" target="_blank">Blues Barber Shop</a> </li>
          <li><i className="fas fa-user-tie"></i> Camicie che non si stirano (sobrie)</li>
        </ul>

        <h2>Under 50€</h2>
        <ul>
          <li><i className="fas fa-guitar"></i> <a href="https://www.amazon.it/Jim-Dunlop-482P-Plettro-confezione/dp/B00JFGPYWY/" target="_blank">Plettri da chitarra 1</a> <i className="fas fa-boxes"></i> <i className="fab fa-amazon"></i></li>
          <li><i className="fas fa-guitar"></i> <a href="https://www.amazon.it/Jim-Dunlop-427PJP-Plettri-Spessore/dp/B0117TLSZS" target="_blank">Plettri da chitarra 2</a> <i className="fas fa-boxes"></i> <i className="fab fa-amazon"></i></li>
          <li><i className="fas fa-user-tie"></i> <a href="https://www.amazon.it/dp/B06XDMYRTR" target="_blank">Acqua di colonia Proraso Azure Lime</a> <i className="fas fa-boxes"></i> <i className="fab fa-amazon"></i></li>
          <li><i className="fas fa-guitar"></i> <a href="https://www.amazon.it/Jim-Dunlop-6504-della-chitarra/dp/B0007V5Z2A/" target="_blank">Kit Pulizia Chitarra</a> <i className="fas fa-box"></i> <i className="fab fa-amazon"></i></li>
        </ul>
        <h2>Over 100€</h2>
        <ul>
          <li><i className="fas fa-user-tie"></i> <a href="https://it.velasca.com/products/arcevia-boc" target="_blank">Camicia Velasca Arcevia - Bianco - Tg. 44</a> <i className="fas fa-box"></i></li>
          <li><i className="fas fa-microchip"></i> <a href="https://www.elgato.com/it/green-screen" target="_blank">Elgato Green Screen</a> <i className="fas fa-box"></i></li>
          <li><i className="fas fa-microchip"></i> <a href="https://www.elgato.com/it/multi-mount-system#configurator" target="_blank">Elgato Master Mount L + Flex Arm L</a> <i className="fas fa-box"></i></li>
          <li><i className="fas fa-utensils"></i> <a href="https://www.sageappliances.com/eu/it/products/coffee/bdc400.html?sku=SDC400BSS4EEU1" target="_blank">Sage Precision Brewer Glass</a> <i className="fas fa-box"></i></li>
          <li><i className="fas fa-utensils"></i> <a href="https://www.sageappliances.com/eu/it/products/coffee-grinders/bcg820.html?sku=SCG820BSS4EEU1" target="_blank">Sage Smart Grinder Pro</a> <i className="fas fa-box"></i></li>
          <li><i className="fas fa-microchip"></i> <a href="https://www.apple.com/it/shop/product/MQD83TY/A/airpods-pro" target="_blank">Apple AirPods Pro (II Gen)</a> <i className="fas fa-box"></i></li>
        </ul>

        <h2>Legenda</h2>
        <table>
          <tr><td>
            <ul>
              <li><i className="fas fa-box"></i> - Regalo una tantum</li>
              <li><i className="fas fa-boxes"></i> - Regalo ripetibile</li>
              <li><i className="fab fa-amazon"></i> - Disponibile su Amazon</li>
            </ul>
          </td><td>
            <ul>
              <li><i className="fas fa-utensils"></i> - Cucina</li>
              <li><i className="fas fa-wine-bottle"></i> - Drinks</li>
              <li><i className="fas fa-user-tie"></i> - Fashon</li>
            </ul>
          </td><td>
            <ul>
              <li><i className="fas fa-motorcycle"></i> - Moto</li>
              <li><i className="fas fa-guitar"></i> - Musica</li>
              <li><i className="fas fa-microchip"></i> - Tecnologia</li>
            </ul>
          </td></tr>
        </table>
      </div>
    </div>
  );
}
