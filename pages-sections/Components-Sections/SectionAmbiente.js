import React from "react";
// plugin that creates slider
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
// @mui/icons-material
import Favorite from "@mui/icons-material/Favorite";
import People from "@mui/icons-material/People";
import Check from "@mui/icons-material/Check";
import FiberManualRecord from "@mui/icons-material/FiberManualRecord";
// core components
import styles from "../../styles/jss/nextjs-material-kit/pages/componentsSections/basicsStyle.js";

const useStyles = makeStyles(styles);

export default function SectionAmbiente() {
  const classes = useStyles();
  return (
    <div className={classes.sections}>
      <div className={classes.container}>
          <p>In questa pagina elenco alcuni progetti interessanti che ho trovato a tutela dell’ambiente e che ho deciso di sostenere.</p>
            <h2>3Bee</h2>
            <a href="https://www.3bee.it/" target="_blank" rel="noreferrer noopener">https://www.3bee.it/</a>
            <p>É una azienda agri-tech che sviluppa sistemi intelligenti di monitoraggio e diagnostica per gli animali. Il suo primo focus sono state le api e la biodiversità. Unendo elettronica e biologia hanno sviluppato un sistema di monitoraggio dello stato di salute degli alveari. Il sistema permette di analizzare i bisogni e le necessità delle api così da intervenire in modo mirato in caso di problemi. Dal loro sito é anche possibile adottare un’arnia a distanza.</p>

            <h2>Treedom</h2>
            <a href="https://www.treedom.net/" target="_blank" rel="noreferrer noopener">https://www.treedom.net/</a>
            <p>Treedom é l’unica piattaforma web al mondo che permette di piantare un albero a distanza e seguirlo online. Dalla sua fondazione, avvenuta nel 2010 a Firenze, sono stati piantati più di 1.000.000 di alberi in Africa, America Latina, Asia e Italia. Tutti gli alberi vengono piantati direttamente da contadini locali e contribuiscono a produrre benefici ambientali, sociali ed economici. Grazie a tale business model, Treedom fa parte dal 2014 delle Certified B Corporations, il network di imprese che si contraddistinguono per elevate performance ambientali e sociali. Ogni albero di Treedom ha una pagina online, viene geolocalizzato e fotografato, può essere custodito o regalato virtualmente a terzi. Grazie a queste caratteristiche, l’albero di Treedom coinvolge le persone ed é al tempo stesso uno strumento di comunicazione e marketing per aziende.</p>

            <h2>WWF</h2>
            <a href="https://sostieni.wwf.it/adotta-una-specie.html" target="_blank" rel="noreferrer noooener">https://sostieni.wwf.it</a>
            <p>Il WWF permette di adottare (ovviamente a distanza) una specie a rischio di estinzione, io ho scelto ovviamente il pinguino ed adesso un bellissimo peluche svetta sulla mia scrivania, cosa aspettate?</p>
      </div>
    </div>
  );
}
