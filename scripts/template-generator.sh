#!/bin/bash

export $(awk '{ print toupper($1)"_VERSION="$2 }' .tool-versions | xargs)
envsubst < Dockerfile.tmpl > Dockerfile
envsubst < package.json.tmpl > package.json
